<?php

include('item.php');
include('itemAuction.php');
include('bid.php');

class Auction{
	
	public $items = array(); 

	//add new item to list of items for auction
	public function addItem($name,$price){
		if ($this->itemExists($name)){
			return "Item already exists. Cannot add duplicate item.";
		}
		$this->items[$name] = new Item($name,$price);
		return "Item successfully added.";
	}

	public function itemExists($name){
		return array_key_exists($name,$this->items);
	}

	public function auctionExists($name){
		$current_item = $this->items[$name];
		return (!is_null($current_item->itemAuction));
	}

	//start itemAuction on a certain item
	public function startAuction($name){
		if (!$this->itemExists($name)){
			return "Item still hasn't been added.";
		}
		if ($this->auctionExists($name)){
			return "Item already has been auctioned.";
		}
		$current_item = $this->items[$name];
		$current_item->itemAuction = new itemAuction();
		return "Auction on ".$name." successfully started.";
	}

	//submit bid for a certain item Auction
	public function submitBid($name,$price,$bidder){
		if (!$this->itemExists($name)){
			return "Item still hasn't been added.";
		}
		if (!$this->auctionExists($name)){
			return "Item auction has not been started yet.";
		}
		$current_item = $this->items[$name];
		$current_item_auction = $current_item->itemAuction;
		$bid = new Bid($price, $bidder);
		$result = $current_item_auction->addBid($bid);
		return ($result ? 'Bid Successful.' : 'Bid must be greater than last bid.');
	}

	//allow user to check the status of a particular item
	public function lookupItem($name){

		//is item in item list
		if (!$this->itemExists($name)){
			return "Item not defined.";
		}

		//was itemAuction ever started 
		if (!$this->auctionExists($name)){
			return "Item not currently being auctioned.";
		}

		$current_item = $this->items[$name];
		$current_item_auction = $current_item->itemAuction;

		//is auction still active?
		if ($current_item_auction->status){
			return "Item is still in auction. Most recent bid is $ ".$current_item_auction->getLastBid().".";
		}

		//grab winning bid and return it
		$winningBid = $current_item_auction->winningBid;
		return "Auction completed. Winning bid is ".$winningBid->price." by ".$winningBid->bidder.".";
	}

	//function to end an auction on an item
	public function endAuction($name){

		$auctionSuccess = false; 

		//ensure item actually exists
		if (!$this->itemExists($name)){
			return "Can't end an auction for an item that doesn't exist!";
		}

		//ensure auction exists
		if (!$this->auctionExists($name)){
			return "Can't end an auction that hasn't begun!";
		}
		//is reserved price greater than final bid? 
		$current_item = $this->items[$name];
		$current_item_auction = $current_item->itemAuction; 
		if ($current_item_auction->getLastBid()->price > $current_item->price){
			$auctionSuccess = true;
		}

		$current_item_item->endAuction($auctionSuccess);

		$appendString =  ($auctionSuccess ? "success" : "failure");
		
		return "Auction on item ".$name." ended. Auction was a ".$appendString.".";
	}

}

?>