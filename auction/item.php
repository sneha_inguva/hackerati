<?php

class Item {
    
    private $name;
    private $price;
    private $itemAuction = NULL;
    
    function __construct($name, $price){
        $this->name = $name;
        $this->price = $price;
    }
    
    /*Function get method*/
    public function __get($name){
    	return $this->$name;
    }
    /*Function set method*/
    public function __set($name, $value){
    	$this->$name = $value;
    }

}

?>