<?php 

include ('auction.php');

assert_options(ASSERT_ACTIVE, 1);
assert_options(ASSERT_WARNING, 0);
assert_options(ASSERT_QUIET_EVAL, 1);
assert_options(ASSERT_CALLBACK,'my_assert_handler');

function my_assert_handler($file, $line, $code)
{
    echo "Assertion Failed: '$file' Line '$line'";
}


$auction = new Auction();

//item properly being added
assert($auction->addItem("vase", 500) == "Item successfully added.");

//duplicate item not allowed to be added
assert($auction->addItem("vase", 500) == "Item already exists. Cannot add duplicate item.");

//a new item can be added
assert($auction->addItem("pumpkin",200) == "Item successfully added.");

//a bid cannot be created until an item is added or an auction is started
assert($auction->submitBid("pear", 250, "Elon musk") == "Item still hasn't been added.");
assert($auction->submitBid("pumpkin", 250, "Elon musk") == "Item auction has not been started yet.");

//start auction
assert($auction->startAuction("pumpkin") == "Auction on pumpkin successfully started.");

//add first bid to auction
assert($auction->submitBid("pumpkin", 250, "Elon musk") == "Bid Successful.");

//add small bid to auction
assert($auction->submitBid("pumpkin", 20, "Elon musk") == "Bid must be greater than last bid.");

//add a better bid to auction
assert($auction->submitBid("pumpkin", 300, "Elon musk") == "Bid Successful.");

//end the auction
assert($auction->endAuction("pumpkin") == "Auction on item pumpkin ended. Auction was a success.");

assert($auction->lookupItem("pumpkin") == "Auction completed. Winning bid is 300 by Elon musk.");

?>