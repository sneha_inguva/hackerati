<?php

class Bid {
	
	private $price;
	private $bidder;

	function __construct($price, $bidder){
        $this->price = $price;
        $this->bidder = $bidder;
    }

    /*Function get method*/
    public function __get($name){
    	return $this->$name;
    }
    /*Function set method*/
    public function __set($name, $value){
    	$this->$name = $value;
    }
}

?>