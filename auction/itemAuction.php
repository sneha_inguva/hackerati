<?php

class itemAuction{
	
	private $bids = array();
	private $status; //true - itemAuction active, false - itemAuction inactive
	private $winningBid;
	private $isSuccess; 

	function __construct(){
		$this->status = true;
	}

	/*Function get method*/
    public function __get($name){
    	return $this->$name;
    }
    /*Function set method*/
    public function __set($name, $value){
    	$this->$name = $value;
    }

    public function addBid($bid){
    	$canAdd = false;

        //if there are currently no bids - can immediately push a bid
        if (!$this->getLastBid()){
            array_push($this->bids, $bid);
            $canAdd = true;
            return $canAdd;
        }

        //if there are bids, make sure they are greaer than previous bids
    	if ($bid->price > $this->getLastBid()->price) {
    		$canAdd = true;
    		array_push($this->bids, $bid);
    	}
    	return $canAdd;
    }

    public function getLastBid(){
        if (empty($this->bids)){
            return false;
        }
    	return end($this->bids);
    }

    public function endAuction($isSuccess){
    	$this->status = false; 
    	$this->winningBid = end($this->bids);
    	$this->isSuccess = $isSuccess;
    }

}

?>