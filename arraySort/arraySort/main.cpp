//
//  main.cpp
//  arraySort
//
//  Created by Sneha Inguva on 2/7/16.
//  Copyright © 2016 Sneha Inguva. All rights reserved.
//

#include <iostream>
#include <vector>
#include <stdlib.h>
using namespace std;

//relevant helper function
bool checkRun(int array[], int i, bool isAscent){
    int count = 0;
    bool isRun = true;
    int check = (isAscent ? 1 : -1);
    while (count < 2 ){
        if (array[i+count+1] != array[i+count] + check){
            isRun = false;
            break;
        }
        count++;
    }
    return isRun;
}

//First attempt at this - O(3N) time (can be O(N^2) worst case)
vector<int> findConsecutiveRuns(int array[], int array_size){
    std::vector<int> r;
    //iterate through the array and find runs of at least 3
    for(int i = 0; i < array_size-2; i++){
        if (array[i+1] == array[i]+1){
            if (checkRun(array, i, true)){
                r.push_back(i);
            }
        }
        if (array[i+1] == array[i]-1){
            if (checkRun(array,i, false)){
                r.push_back(i);
            }
        }
    }
    
    return r; //C++ - cannot return NULL if no runs found but can return empty vector
}

int main(int argc, const char * argv[]) {
    
    int test[] = {1, 2, 3, 5, 10, 9, 8, 9, 10, 11, 7};
    int array_size = 11;
    
    std::cout << "BEGINNING THE PROGRAM" << std::endl;
    std::vector<int> result = findConsecutiveRuns(test, array_size);
    
    //Printing out the results of the program
    std::cout << "RESULTS" << std::endl;
    if (result.size()){
        for (int i = 0; i < result.size(); i++){
            std::cout << result[i] << " ";
        }
    }else{
        std::cout << "NO RESULTS" << std::endl;
    }

    return 0;
    
}
